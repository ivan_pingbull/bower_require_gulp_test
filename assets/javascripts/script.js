jQuery(function($){
    var body = $('body'),
        header = $('#header'),
        headerMenu = $('.header-menu'),
        globalWrap = $('.global-wrap'),
        menu = $('#menu'),
        menuClose = $('.menu-close'),
        overlay = $('.overlay'),
        pageContainer = $('.page-container'),
        formWrap = $('.gform_wrapper'),
        search = $('.search'),
        video = $('.video'),
        videoPlay = $('.video-play'),
        iframe = $('iframe'),
        container = $('.container'),
        sidebar = $('aside'),
        menuContainer = $('.menu-container');


    /*WPADMINBAR*/
    var wpadminbar= $('#wpadminbar'),
        adminHeight = wpadminbar.outerHeight();

    if(adminHeight === null){
        adminHeight=0;
    }


	/*IE8 ICONS FIX*/
    $('head').find('style#ieIcons').remove();

    /*Placeholder*/
    $('input, textarea').placeholder();
    $(document).ajaxStop(function(){
        $('input, textarea').placeholder();
    });



    /*HEADER STICKY*/

    $(window).on("scroll", function(){
        if($(window).scrollTop() > 0 && !header.hasClass('sticky')){
            header.addClass('sticky');
        }
        if($(window).scrollTop() == 0 && header.hasClass('sticky')){
            header.removeClass('sticky');
        }
    });


    /*SIDEBAR HEIGHT*/
    function sidebarHeight(){
        if(sidebar.height()<=container.height()){
            sidebar.css("min-height",container.height());            
        }
        else{
            sidebar.css("min-height","auto");
        }
    }

    setTimeout(function(){
        sidebarHeight();
    },100)

    $(window).resize(function(){
        setTimeout(function(){
            sidebar.css("min-height",container.height());
        },1000)       
    });


    /*MENU*/

    headerMenu.click(function(){
        $(this).toggleClass('menu-open');
        globalWrap.toggleClass('menu-open');
        menu.toggleClass('menu-open');
        overlay.toggleClass('open');
    });


    overlay.click(function() {
        $(this).removeClass('open');
        headerMenu.removeClass('menu-open');
        globalWrap.removeClass('menu-open');
        menu.removeClass('menu-open');
    });

    menuClose.click(function(event) {
        event.preventDefault();
        overlay.removeClass('open');
        headerMenu.removeClass('menu-open');
        globalWrap.removeClass('menu-open');
        menu.removeClass('menu-open');
    });









    /*List-news, List-project,  Show-more button*/

    var list = pageContainer.find('.two-col[data-list-size]'),
        listItem = list.find('li'),
        showMore = list.siblings('.show-more'),
        showOrHideList = true,
        listSizeBase = list.attr("data-list-size");


    listItem.eq(0).addClass('left');
    listItem.eq(1).addClass('right');
    for(var i=2;i<listItem.length;i++){
        var listLeft = list.find('.left');
        var listLeftHeight=0;
        for(var j=0;j<listLeft.length;j++){
            listLeftHeight+=listLeft.eq(j).outerHeight();
        }
        var listRight = list.find('.right');
        var listRightHeight=0;
        for(var k=0;k<listRight.length;k++){
            listRightHeight+=listRight.eq(k).outerHeight();
        }
        if(listLeftHeight<=listRightHeight){
            listItem.eq(i).addClass('left');
        }
        else{
            listItem.eq(i).addClass('right');            
        }
    }

    if(listItem.length<listSizeBase){
        showMore.hide();
    }

    list.find('li:lt(' + listSizeBase + ')').addClass('show');

    showMore.click(function (event) {
        event.preventDefault();
        if (showOrHideList == true) {
            showOrHideList = false;
            list.find('li').addClass('show');
            showMore.addClass('show-more-open');
        } else {
            showOrHideList = true;
            list.find('li').removeClass('show');
            list.find('li:lt(' + listSizeBase + ')').addClass('show');
            showMore.removeClass('show-more-open');
        }
    });






    /*Search results*/
    
    var searchResult = $('.search-result'),
        resultBaseSize = searchResult.attr('data-result-size'),
        showResult = searchResult.siblings('.show-more'),
        showOrHideResult = true;


    if(searchResult.find('.post').length<resultBaseSize){
        showResult.hide();
    }

    searchResult.find('.post:lt(' + resultBaseSize+ ')').addClass('show');
    setTimeout(function(){
        sidebar.css("min-height",container.height()); 
    },100)

    showResult.click(function (event) {
        event.preventDefault();
        if (showOrHideResult == true) {
            showOrHideResult = false;
            searchResult.find('.post').addClass('show');
            showResult.addClass('show-more-open');
        } else {
            showOrHideResult = true;
            searchResult.find('.post').removeClass('show');
            searchResult.find('.post:lt(' + resultBaseSize+ ')').addClass('show');
            showResult.removeClass('show-more-open');
        }

        sidebar.css("min-height",container.height()); 

    });



    /*INPUT FOCUS*/

    if(body.css("content")=="ipad" || body.css("content")=="\"ipad\""){
        function pseudoFixed(TrueOrFalse){
            if(TrueOrFalse==false){

                // overlay.removeClass('open');
                body.removeClass('inputFocus');
                header.css({
                    "top": adminHeight + "px"
                });
                menu.css({
                    "top": adminHeight + "px"
                });
                $(window).on('scroll mousewheel', function(){
                    header.css({
                        "top": adminHeight + "px"
                    });
                    menu.css({
                        "top": adminHeight + "px"
                    });
                });
                $(window).unbind('touchmove');            
                

            } else{
                
                // overlay.addClass('open');
                body.addClass('inputFocus');
                header.css({
                    "top": $(window).scrollTop() + adminHeight + "px"
                });
                menu.css({
                    "top": $(window).scrollTop() + adminHeight + "px"
                });
                $(window).on('scroll mousewheel', function(){
                    header.css({
                        "top": $(window).scrollTop() + adminHeight + "px"
                    });
                    menu.css({
                        "top": $(window).scrollTop() + adminHeight + "px"
                    });
                });
                $(window).on('touchmove', function(event){
                    event.preventDefault();
                });
                // $("body, html").animate({
                //     scrollTop: $(window).scrollTop() + 55 + "px"
                // },500)

            }
        }


        $('input, textarea').focus(function(){
            pseudoFixed();      
        });
        $('input, textarea').blur(function(){
            pseudoFixed(false);
            setTimeout(function(){
                $(window).scrollTop($(window).scrollTop()+1);
                $(window).scrollTop($(window).scrollTop()-1);
            },500)      
        });

        overlay.click(function(){
            pseudoFixed(false);
        });

        $(window).on('orientationchange', function(){
            $('input, textarea').blur();
            pseudoFixed(false);
        });

        formWrap.on("click", function(){
            $('input, textarea').blur();
            pseudoFixed(false);        
        });
        search.on("click", function(){
            $('input, textarea').blur();
            pseudoFixed(false);        
        });
        menuContainer.on("click", function(){
            $('input, textarea').blur();
            pseudoFixed(false);        
        });

        $('input, textarea').on("click", function(event){
            event.stopPropagation();
        });

        $('input[type=submit]').focus(function(){
            pseudoFixed(false);
        });        
    }





    /*Event widget style kill*/
    $("head").find("#ai1ec_style-css").remove();


     /*VIDEO*/

    video.addClass('play');
    player(iframe);


    videoPlay.on('click', function (event) {
        video.addClass('play');
        player(iframe);
    });


     function player($iframe) {
      var url = $iframe.attr('src'),
       match = url.match(/(youtu.be|youtube|vimeo)/g),
       youtubeId = 'youtube-api',
       vimeoId = 'vimeo-api',
       player = null,
       script;

      if (match[0]) {
       script = document.createElement('script');

       if (match[0] === 'youtube' || match[0] === 'youtu.be') {

        if (!document.getElementById(youtubeId)) {
         script.src = '//www.youtube.com/iframe_api';
         script.id = youtubeId;
         $('head').append(script);

         window.onYouTubeIframeAPIReady = function() {
          player = new YT.Player($iframe[0], {
           events: {
            onReady: function (event) {
             event.target.playVideo();
             $iframe.data('player', event.target);
             video.addClass('play');
            },
            onStateChange: function(event){
                if(event.data == 0){
                    video.removeClass('play');                    
                }
            }
           }
          });
         }
        } else if ($iframe.data('player')) {
         $iframe.data('player').playVideo();
         video.addClass('play');
        } else {
         player = new YT.Player($iframe[0], {
          events: {
           onReady: function (event) {
            event.target.playVideo();
            $iframe.data('player', event.target);
            video.addClass('play');
           },
            onStateChange: function(event){
                if(event.data == 0){
                    video.removeClass('play');                    
                }
            }
          }
         });
        }

       } else {

        if (!document.getElementById(vimeoId)) {
         script.src = '//f.vimeocdn.com/js/froogaloop2.min.js';
         script.id = vimeoId;
         $('head').append(script);

         var vimeoAPIInterval = setInterval(function () {

          if (typeof $f !== 'undefined' && $f) {
           clearInterval(vimeoAPIInterval);
           player = $f($iframe[0]);
           player.addEvent('ready', function() {
            player.api('play');
            video.addClass('play');
            player.addEvent('finish', function(){
              video.removeClass('play');            
            });
           });
          }
         }, 100);
        } else {
         player = $f($iframe[0]);
         player.addEvent('ready', function () {
          player.api('play');
          video.addClass('play');
          player.addEvent('finish', function(){
            video.removeClass('play');            
          });
         });
        }
        
       }
      }

      return null;
     }
   
});