requirejs.config({
    paths:{
        "jquery": "../bower_components/jquery/dist/jquery.min",
        "placeholder" : "../bower_components/jquery.placeholder/jquery.placeholder.min",
        "script": "scripts.min"
    },
    shim: {
    	"placeholder": {
    		exports: 'jquery'
    	},
    	"script": {
    		exports: 'jquery'
    	}
    }
});


require(["jquery","placeholder","script"], function(){});